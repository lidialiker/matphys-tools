#!/bin/python

from numpy import *
from matrix_gen_utils import *
from optparse import OptionParser

parser = OptionParser()

parser.add_option('-f', '--filename', default='matrix_gen_in.txt',
    dest='filename', help='Full path to file containing input parameters\
                           which should be specified in following format :\
                           M #count of members in sum\
                           U_1 K_1\
                           # ...  \
                           U_N K_N\
                           where harmonics fall into class U_i * cos(K_i*x)')

parser.add_option('-o', '--output', default='matrix_gen_out.npy',
    dest='output', help='Full path to file resulting hamiltonian to be written')

parser.add_option('-q', '--quiet', default=False,
    dest='quiet', help='Truncates verbose output')

options, args = parser.parse_args()

def hamiltonian(M, harmonic_params):
    result = zeros((2*M + 1, 2*M + 1))
    result[0,0] = - 1.0 ** 2 / 2;
    for i in xrange(1, 2*M + 1):
        result[i,i] = - (i + 1) ** 2 / 2
    for u, k in harmonic_params.iteritems():
        for i in xrange(2*M + 1):
            if i - k >= 0:
                result[i, i - k] = u / 2.0
            if i + k < 2*M + 1:
                result[i, i + k] = u /2.0
    return result

M, harmonic_params = readfile(options.filename, options.quiet, parser.print_help)
H = hamiltonian(M, harmonic_params)

if not options.quiet:
    print "Hamiltonian ..."
    print H

save(options.output, H)
