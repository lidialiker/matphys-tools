import contextlib

@contextlib.contextmanager
def checkwrongformat(callback):
    try:
        yield
    except:
        print "Wrong file format!"
        callback()
        exit(-1)

def readfile(filename, quiet, callback):
    with open(filename, 'rt') as f:
        with checkwrongformat(callback):
            M = int(f.readline().strip())
            if not quiet:
                print "M = %d" % M

        harmonic_params = dict()
        while True:
            l = f.readline()
            if l != '':
                tmp = l.strip().split()
                with checkwrongformat(callback):
                    u = float(tmp[0])
                    k = int(tmp[1])

                harmonic_params[k] = u
            else:
                break

        if not quiet:
            print "U(x) = "
            for u, k in sorted(harmonic_params.iteritems()):
                print " %.3f * cos(%d*x)" % (u, k)
                print " + "

    return M, harmonic_params
